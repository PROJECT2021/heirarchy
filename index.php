<?php include 'config.php';?>
<?php
    $data   = [];
    $query  = $db->query("SELECT * FROM binary_points WHERE id_number = '1665163' LIMIT 1");
    $user   = $query->fetch_object();
    $result = $db->query("SELECT * FROM binary_points WHERE level > 0 AND id_number = ".$user->id_number);
    while( $row = $result->fetch_assoc() ) { 
        $tmp = array();
        $tmp['id_number']         = $row['id_number'];
        $tmp['waiting_id_number'] = $row['waiting_id_number'];
        $tmp['upline_id']         = $row['upline_id'];
        $tmp['level']             = $row['level'];
        array_push($data, $tmp); 
    }

    foreach($data as $key => $value) {

        $array[$value['level']][] = ['id_number' => $value['id_number'],'waiting_id_number' => $value['waiting_id_number'],'upline_id' => $value['upline_id'],'level' => $value['level']];
        
    }

    // echo '<pre>';
    // print_r($array);
    // echo '</pre>';
    // exit;
?>

<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>jOrgChart - A jQuery OrgChart Plugin</title>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/jquery.jOrgChart.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <link href="css/prettify.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="prettify.js"></script>

    <!-- jQuery includes -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js">
    </script>

    <script src="jquery.jOrgChart.js"></script>

    <script>
        jQuery(document).ready(function () {
            $("#org").jOrgChart({
                chartElement: '#chart',
                dragAndDrop: true
            });
        });
    </script>
</head>

<body onload="prettyPrint();">
    <?php $level = 1;?>
    <ul id="org" style="display:none">
        <li><?=$user->id_number?>
            <ul>
                <?php foreach($array as $key => $value) { ?>
                    <?php if($key > 0) { ?>
                        <?php foreach($value as $data) { ?>
                            <?php if($data['level'] == 1) { ?>
                                <li><?=$data['waiting_id_number']?>
                            <?php } else { ?>
                                <?php if($data['level'] > 1 && $data['waiting_id_number'] == $data['upline_id']) { ?>
                                    <li>22</li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>   
            </ul>
        </li>
    </ul>

    <div id="chart" class="orgChart"></div>

    <script>
        jQuery(document).ready(function () {

            /* Custom jQuery for the example */
            $("#show-list").click(function (e) {
                e.preventDefault();

                $('#list-html').toggle('fast', function () {
                    if ($(this).is(':visible')) {
                        $('#show-list').text('Hide underlying list.');
                        $(".topbar").fadeTo('fast', 0.9);
                    } else {
                        $('#show-list').text('Show underlying list.');
                        $(".topbar").fadeTo('fast', 1);
                    }
                });
            });

            $('#list-html').text($('#org').html());

            $("#org").bind("DOMSubtreeModified", function () {
                $('#list-html').text('');

                $('#list-html').text($('#org').html());

                prettyPrint();
            });
        });
    </script>

</body>

</html>